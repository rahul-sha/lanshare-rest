This is a REST server for sharing files in local LAN system with an IP based security enforced. This is not a ground breaking product or anything. This is a little usecase which I use in real life. But don't let that small UC fool you as this is my playground of technologies and there is much of it.

1. Spring framework
2. Hibernate
3. Spring Data
4. Spring Security
5. Maven
6. REST API
7. Java 8 (in some places even 9 or 10 might be used as this is built on JDK 10)
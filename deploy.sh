#!/usr/bin/env bash
set -e
mvn clean install
CATALINA_HOME=/home/rahul/Packages/apache-tomcat-9.0.8
# sh $CATALINA_HOME/bin/shutdown.sh
cp target/lanshare.war $CATALINA_HOME/webapps/
rm -rf $CATALINA_HOME/webapps/lanshare
sh $CATALINA_HOME/bin/catalina.sh run


package repository;


import com.lanshare.configuration.ApplicationConfiguration;
import com.lanshare.entity.MessageEntity;
import com.lanshare.entity.UserEntity;
import com.lanshare.enums.MessageStatus;
import com.lanshare.repository.MessageRepository;
import com.lanshare.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
@PropertySource("classpath:jdbc.properties")
@Transactional
public class MessageRepositoryTest{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Test
    @Rollback
    public void testSave(){
        UserEntity sender = new UserEntity();
        sender.setName("snd");
        userRepository.save(sender);

        UserEntity receiver = new UserEntity();
        receiver.setName("rcv");
        userRepository.save(receiver);

        MessageEntity messageEntity = new MessageEntity(sender, receiver, "Hello",
                System.currentTimeMillis(), MessageStatus.UNREAD.name());

        messageEntity.setMessagePk(1);
        messageRepository.save(messageEntity);
    }
}

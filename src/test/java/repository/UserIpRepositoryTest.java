package repository;

import com.lanshare.configuration.ApplicationConfiguration;
import com.lanshare.entity.UserAuthorityEntity;
import com.lanshare.entity.UserEntity;
import com.lanshare.entity.UserIpEntity;
import com.lanshare.repository.UserIpRepository;
import com.lanshare.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
@PropertySource("classpath:jdbc.properties")
@Transactional
public class UserIpRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserIpRepository userIpRepository;

    @Test
    @Rollback
    public void testFindUserEntityByIp(){
        UserEntity userEntity = new UserEntity();
        userEntity.setName("john");

        UserIpEntity userIpEntity = new UserIpEntity();
        userIpEntity.setIp("127.0.0.1");
        userIpEntity.setUserEntity(userEntity);

        UserAuthorityEntity userAuthorityEntity = new UserAuthorityEntity();
        userAuthorityEntity.setAuthority("ROLE_ADMIN");
        userAuthorityEntity.setUserEntity(userEntity);

        userEntity.setUserAuthorities(Collections.singleton(userAuthorityEntity));
        userEntity.setUserIpEntities(Collections.singleton(userIpEntity));

        userRepository.save(userEntity);

        Optional<UserIpEntity> optionalUserIpEntity = userIpRepository.findByIp("127.0.0.1");

        Assert.assertTrue(optionalUserIpEntity.isPresent());
        UserEntity actualUserEntity = optionalUserIpEntity.get().getUserEntity();

        Assert.assertNotNull(actualUserEntity);
        Assert.assertEquals("john",actualUserEntity.getName());

        Assert.assertEquals(1, actualUserEntity.getUserAuthorities().size());
        Assert.assertEquals("ROLE_ADMIN", actualUserEntity
                .getUserAuthorities().iterator().next().getAuthority());

        Assert.assertEquals(1, actualUserEntity.getUserIpEntities().size());
        Assert.assertEquals("127.0.0.1", actualUserEntity
                .getUserIpEntities().iterator().next().getIp());
    }
}

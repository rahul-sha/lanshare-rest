package service;

import com.lanshare.configuration.ApplicationConfiguration;
import com.lanshare.domain.Message;
import com.lanshare.entity.UserEntity;
import com.lanshare.enums.MessageStatus;
import com.lanshare.exception.LSException;
import com.lanshare.exception.ValidationException;
import com.lanshare.repository.UserRepository;
import com.lanshare.service.MessageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
@PropertySource("classpath:jdbc.properties")
@Transactional
public class MessageServiceTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageService messageService;


    @Test
    public void testMessage() throws ValidationException {
        UserEntity sender = new UserEntity();
        sender.setName("snd");

        UserEntity receiver = new UserEntity();
        receiver.setName("rcv");

        userRepository.save(sender);
        userRepository.save(receiver);

        Message message = new Message("snd", "rcv", "Hello world",
                System.currentTimeMillis(), MessageStatus.UNREAD.name());

        messageService.writeMessage(message);
    }
}

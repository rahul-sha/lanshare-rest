package service;

import com.lanshare.configuration.ApplicationConfiguration;
import com.lanshare.domain.User;
import com.lanshare.entity.UserAuthorityEntity;
import com.lanshare.entity.UserEntity;
import com.lanshare.entity.UserIpEntity;
import com.lanshare.repository.UserRepository;
import com.lanshare.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
@PropertySource("classpath:jdbc.properties")
@Transactional
public class UserServiceTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Test
    public void testGetUserFromIp() {
        UserEntity userEntity = new UserEntity();

        UserAuthorityEntity userAuthorityEntity = new UserAuthorityEntity();
        userAuthorityEntity.setAuthority("ROLE_ADMIN");
        userAuthorityEntity.setUserEntity(userEntity);

        UserIpEntity userIpEntity = new UserIpEntity();
        userIpEntity.setIp("127.0.0.1");
        userIpEntity.setUserEntity(userEntity);

        Set<UserAuthorityEntity> userAuthorities = new HashSet<>();
        userAuthorities.add(userAuthorityEntity);

        Set<UserIpEntity> userIps = new HashSet<>();
        userIps.add(userIpEntity);

        userEntity.setName("John");
        userEntity.setUserAuthorities(Collections.unmodifiableSet(userAuthorities));
        userEntity.setUserIpEntities(Collections.unmodifiableSet(userIps));

        userRepository.save(userEntity);

        User user = userService.getUserFromIp("127.0.0.1");

        Assert.assertEquals(user.getName(), "John");
        Assert.assertEquals(user.getIps(), userIps.stream().map(UserIpEntity::getIp).collect(Collectors.toSet()));
        Assert.assertEquals(user.getAuthorities(), userAuthorities.stream().
                map(UserAuthorityEntity::getAuthority)
                .collect(Collectors.toSet()));
    }

    @Test(expected = AssertionError.class)
    public void testGetUserByNameError() {
        userService.getUserByName("foobar");
    }

    @Test
    public void testGetAllUserNames() {
        UserEntity user1 = new UserEntity();
        user1.setName("user1");

        UserEntity user2 = new UserEntity();
        user2.setName("user2");

        userRepository.save(user1);
        userRepository.save(user2);

        List<String> userNameList = userService.getAllUserNames();
        Assert.assertEquals(2, userNameList.size());
        Assert.assertEquals(Arrays.asList("user1", "user2"), userNameList);
    }

}

package controller;

import com.lanshare.controller.UserController;
import com.lanshare.domain.User;
import com.lanshare.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.HashSet;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerTest {

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;

    private MockMvc mockMvc;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    public void testGetUserName() throws Exception {

        User user = new User();
        user.setName("john");
        user.setIps(new HashSet<>(Collections.singletonList("127.0.0.1")));
        user.setAuthorities(new HashSet<>(Collections.singletonList("ROLE_ADMIN")));

        Mockito.when(userService.getUserByName("john")).thenReturn(user);

        MvcResult result = mockMvc.perform(get("/user").principal(
                new UsernamePasswordAuthenticationToken("john", "127.0.0.1", null)))
                .andExpect(status().isOk())
                .andReturn();

        final String expected = "{name:\"john\",ips:[\"127.0.0.1\"],authorities:[\"ROLE_ADMIN\"]}";
        JSONAssert.assertEquals(expected, result.getResponse()
                .getContentAsString(), false);
    }
}

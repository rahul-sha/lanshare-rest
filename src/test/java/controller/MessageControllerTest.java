package controller;

import com.lanshare.configuration.ApplicationConfiguration;
import com.lanshare.configuration.ServletConfiguration;
import com.lanshare.controller.ControllerExceptionHandler;
import com.lanshare.controller.MessageController;
import com.lanshare.entity.UserEntity;
import com.lanshare.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.NestedServletException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class, ServletConfiguration.class})
@WebAppConfiguration
@PropertySource("classpath:jdbc.properties")
@Transactional
public class MessageControllerTest {

    @Autowired
    private MessageController messageController;

    @Autowired
    private ControllerExceptionHandler controllerExceptionHandler;

    @Autowired
    private UserRepository userRepository;

    private MockMvc mockMvc;

    @Before
    public void init() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(messageController)
                .setControllerAdvice(controllerExceptionHandler)
                .build();
    }

    @Test
    public void testErrorMessage() throws Exception {
        UserEntity sender = new UserEntity();
        sender.setName("john");

        UserEntity receiver = new UserEntity();
        receiver.setName("doe");

        userRepository.save(sender);
        userRepository.save(receiver);

        MvcResult result = mockMvc.perform(post("/message/send")
                .param("receiver", "lua")
                .param("message", "This is a test message")
                .principal(
                        new UsernamePasswordAuthenticationToken("john", "127.0.0.1", null)
                )
        )
                .andExpect(status().isBadRequest())
                .andReturn();

        final String expected = "{errorMessage:\"Invalid receiver id\",errorLevel:\"MAJOR\"}";
        JSONAssert.assertEquals(expected, result.getResponse()
                .getContentAsString(), false);
    }
}

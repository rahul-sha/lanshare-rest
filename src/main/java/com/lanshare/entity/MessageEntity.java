package com.lanshare.entity;

import com.lanshare.domain.Message;
import com.lanshare.enums.MessageStatus;

import javax.persistence.*;

@Entity
@Table(name="COM_MESSAGE")
public class MessageEntity implements DtoMappable<Message> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="MESSAGE_PK")
    private long messagePk;

    @OneToOne
    @JoinColumn(name = "SNDR_USER_PK", nullable = false)
    private UserEntity sender;

    @OneToOne
    @JoinColumn(name = "RECV_USER_PK", nullable = false)
    private UserEntity receiver;

    @Column(name="MESSAGE", nullable = false)
    private String message;

    @Column(name="SENDTIME", nullable = false)
    private long sendTime;

    @Column(name="STATUS", nullable = false)
    private String status;

    public MessageEntity(){

    }

    public MessageEntity(UserEntity sender, UserEntity receiver, String message, long sendTime, String messageStatus) {
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
        this.sendTime = sendTime;
        this.status = messageStatus;
    }

    //<editor-fold desc="Getters and Setters">
    public long getMessagePk() {
        return messagePk;
    }

    public void setMessagePk(long messagePk) {
        this.messagePk = messagePk;
    }

    public UserEntity getSender() {
        return sender;
    }

    public void setSender(UserEntity sender) {
        this.sender = sender;
    }

    public UserEntity getReceiver() {
        return receiver;
    }

    public void setReceiver(UserEntity receiver) {
        this.receiver = receiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getSendTime() {
        return sendTime;
    }

    public void setSendTime(long sendTime) {
        this.sendTime = sendTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    //</editor-fold>

    @Override
    public Message getDto() {
        return new Message(sender.getName(), receiver.getName(), message, sendTime, status);
    }
}

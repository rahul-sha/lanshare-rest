package com.lanshare.entity;

public interface DtoMappable<T> {

    T getDto();

}

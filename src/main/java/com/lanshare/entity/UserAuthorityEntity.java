package com.lanshare.entity;

import javax.persistence.*;

@Entity
@Table(name = "REF_USER_AUTHORITIES")
public class UserAuthorityEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ROLE_PK")
    private long userRolePk;

    @Column(name = "AUTHORITY")
    private String authority;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_PK")
    private UserEntity userEntity;

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public long getUserRolePk() {
        return userRolePk;
    }

    public void setUserRolePk(long userRolePk) {
        this.userRolePk = userRolePk;
    }
}

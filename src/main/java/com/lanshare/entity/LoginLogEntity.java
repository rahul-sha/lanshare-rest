package com.lanshare.entity;

import com.lanshare.domain.LoginLog;

import javax.persistence.*;

@Entity
@Table(name="LOG_LOGIN")
public class LoginLogEntity implements DtoMappable<LoginLog>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="LOGIN_PK")
    private long loginPk;

    @ManyToOne
    @JoinColumn(name = "USER_PK")
    private UserEntity user;

    @Column(name="DATETIME")
    private String datetime;

    @Column(name="DETAILS")
    private String details;

    public long getLoginPk() {
        return loginPk;
    }

    public void setLoginPk(long loginPk) {
        this.loginPk = loginPk;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public LoginLog getDto() {
        return new LoginLog(getUser().getName(), getDatetime(), getDetails());
    }
}

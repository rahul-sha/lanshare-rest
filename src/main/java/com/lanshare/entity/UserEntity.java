package com.lanshare.entity;

import com.lanshare.domain.User;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name="REF_USER")
public class UserEntity implements DtoMappable<User> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="USER_PK")
    private long userPk;

    @Column(name="NAME")
    private String name;

    @OneToMany(mappedBy = "userEntity", cascade = CascadeType.ALL)
    private Set<UserAuthorityEntity> userAuthorities;

    @OneToMany(mappedBy = "userEntity", cascade = CascadeType.ALL)
    private Set<UserIpEntity> userIpEntities;

    //<editor-fold desc="Getters and Setters">
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<UserAuthorityEntity> getUserAuthorities() {
        return userAuthorities;
    }

    public void setUserAuthorities(Set<UserAuthorityEntity> userAuthorities) {
        this.userAuthorities = userAuthorities;
    }

    public long getUserPk() {
        return userPk;
    }

    public void setUserPk(long userPk) {
        this.userPk = userPk;
    }

    public Set<UserIpEntity> getUserIpEntities() {
        return userIpEntities;
    }

    public void setUserIpEntities(Set<UserIpEntity> userIpEntities) {
        this.userIpEntities = userIpEntities;
    }
    //</editor-fold>

    @Override
    public User getDto() {
        User user = new User();
        user.setName(this.getName());
        user.setIps(this.getUserIpEntities().stream().map(UserIpEntity::getIp).collect(Collectors.toSet()));
        user.setAuthorities(this.getUserAuthorities().stream().map(UserAuthorityEntity::getAuthority).
                collect(Collectors.toSet()));
        return user;
    }
}

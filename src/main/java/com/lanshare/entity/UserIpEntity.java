package com.lanshare.entity;

import javax.persistence.*;

@Entity
@Table(name = "REF_USER_IP")
public class UserIpEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_IP_PK")
    private long userIpPk;

    @Column(name = "IP")
    private String ip;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_PK")
    private UserEntity userEntity;

    public long getUserIpPk() {
        return userIpPk;
    }

    public void setUserIpPk(long userIpPk) {
        this.userIpPk = userIpPk;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }
}

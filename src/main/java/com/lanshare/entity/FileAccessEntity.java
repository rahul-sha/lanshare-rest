package com.lanshare.entity;

import com.lanshare.domain.FileAccess;

import javax.persistence.*;

@Entity
@Table(name="LOG_FILE_ACCESS")
public class FileAccessEntity implements DtoMappable<FileAccess> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="FILE_ACCESS_PK")
    private long fileAccessPk;

    @OneToOne
    @JoinColumn(name = "USER_PK", nullable = false)
    private UserEntity user;

    @Column(name="FILE_NAME")
    private String filename;

    @Column(name = "FILE_SIZE")
    private String filesize;

    @Column(name="LOCATION")
    private String location;

    @Column(name="ACTION")
    private String action;

    @Column(name="DATETIME")
    private String datetime;

    public FileAccessEntity() {
    }

    public FileAccessEntity(UserEntity user, String filename, String filesize, String location, String action, String datetime) {
        this.user = user;
        this.filename = filename;
        this.filesize = filesize;
        this.location = location;
        this.action = action;
        this.datetime = datetime;
    }

    //<editor-fold desc="Getters and Setters">
    public long getFileAccessPk() {
        return fileAccessPk;
    }

    public void setFileAccessPk(long fileAccessPk) {
        this.fileAccessPk = fileAccessPk;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilesize() {
        return filesize;
    }

    public void setFilesize(String filesize) {
        this.filesize = filesize;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
    //</editor-fold>

    @Override
    public FileAccess getDto() {
        return new FileAccess(user.getName(), filename, filesize, location, action, datetime);
    }

}

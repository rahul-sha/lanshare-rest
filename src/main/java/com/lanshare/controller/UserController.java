package com.lanshare.controller;

import com.lanshare.domain.FileAccess;
import com.lanshare.domain.User;
import com.lanshare.enums.FileAction;
import com.lanshare.service.FileAccessService;
import com.lanshare.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@RequestMapping("/user")
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private FileAccessService fileAccessService;

    @GetMapping
    public User getUserName(HttpServletRequest request) {
        Principal principal = request.getUserPrincipal();
        if(principal != null){
            return userService.getUserByName(principal.getName());
        }
        return null;
    }

    @GetMapping("file-uploads")
    public Page<FileAccess> getUploads(@RequestParam(required = false, name = "fetch_count", defaultValue = "5")
                                                   int fetchSize, HttpServletRequest request) {
        return fileAccessService.getAccessesOf(request.getUserPrincipal().getName(), FileAction.UPLOAD, 0,
                fetchSize);
    }

    @GetMapping("file-downloads")
    public Page<FileAccess> getDownloads(@RequestParam(required = false, name = "fetch_count", defaultValue = "5")
                                                     int fetchSize, HttpServletRequest request) {
        return fileAccessService.getAccessesOf(request.getUserPrincipal().getName(), FileAction.DOWNLOAD, 0,
                fetchSize);
    }
}

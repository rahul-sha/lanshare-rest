package com.lanshare.controller;

import com.lanshare.domain.FileAccess;
import com.lanshare.domain.User;
import com.lanshare.enums.FileAction;
import com.lanshare.exception.ValidationException;
import com.lanshare.service.FileAccessService;
import com.lanshare.service.FileService;
import com.lanshare.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@RequestMapping("/admin")
@RestController
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private FileService fileService;

    @Autowired
    private FileAccessService fileAccessService;

    @GetMapping("file-access-log")
    public Page<FileAccess> getFileAccessLog(
            @RequestParam(required = false, name = "start", defaultValue = "0") int start,
            @RequestParam(required = false, name = "size", defaultValue = "10") int size) {
        return fileAccessService.getFileAccesses(start, size);
    }

    @Secured("READ_USER")
    @GetMapping("user-list")
    public List<User> getUserList() {
        return userService.getAllUsers();
    }

    @PostMapping("/upload")
    public ResponseEntity uploadFile(@RequestParam(value = "file") MultipartFile file,
                                     @RequestParam(value = "path") String path,
                                     HttpServletRequest request) throws ValidationException {
        try {
            fileService.uploadFile(path, file);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        fileAccessService.addAccess(new FileAccess(request.getUserPrincipal().getName(), file.getOriginalFilename(),
                String.valueOf(file.getSize()), path, FileAction.UPLOAD.name()));

        return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "text/plain").build();
    }

    @GetMapping("user-list/{username}")
    public User getUserDetails(@PathVariable("username") String username) {
        return userService.getUserByName(username);
    }

    @PutMapping("/new-user")
    public String addUser(User user) {
        return "new_user";
    }

}

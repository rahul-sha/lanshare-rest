package com.lanshare.controller;

import com.lanshare.domain.Error;
import com.lanshare.enums.ErrorLevel;
import com.lanshare.exception.LSException;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

@ControllerAdvice(basePackages = "com.lanshare.controller")
@RequestMapping(produces = "application/json")
public class ControllerExceptionHandler {

    private final static Logger logger = Logger.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler(LSException.class)
    public ResponseEntity handleAssertionError(LSException ae) {
        logger.error(ae.getLocalizedMessage(), ae);
        return ResponseEntity.badRequest().body(new Error(ErrorLevel.MAJOR, ae.getLocalizedMessage()));
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity handleIOException(IOException e) {
        logger.error(e.getLocalizedMessage(), e);
        return ResponseEntity.badRequest().body(new Error(ErrorLevel.CRITICAL, e.getLocalizedMessage()));
    }
}

package com.lanshare.controller;

import com.lanshare.domain.Message;
import com.lanshare.enums.MessageStatus;
import com.lanshare.exception.ValidationException;
import com.lanshare.service.MessageService;
import com.lanshare.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping("/message")
@RestController
public class MessageController {

    @Autowired
    private UserService userService;

    @Autowired
    private MessageService messageService;

    @PostMapping("send")
    public void sendMessage(@RequestParam(name = "receiver") String receiver,
                            @RequestParam(name = "message") String messageBody,
                            HttpServletRequest request) throws ValidationException {
        Message message = new Message(request.getUserPrincipal().getName(), receiver, messageBody,
                System.currentTimeMillis(), MessageStatus.UNREAD.name());
        messageService.writeMessage(message);
    }

    @GetMapping("recipients")
    public List<String> getRecipientList() {
        return userService.getAllUserNames();
    }

    @GetMapping("receive")
    public Page<Message> getIncomingMessages(@RequestParam(required = false, name = "start", defaultValue = "0") int start,
                                             @RequestParam(required = false, name = "size", defaultValue = "10") int size,
                                             HttpServletRequest request) throws ValidationException {
        return messageService.getMessages(request.getUserPrincipal().getName(),
                PageRequest.of(start, size, Sort.by(new Sort.Order(Sort.Direction.DESC, "sendTime"))));
    }

    @GetMapping("sent")
    public Page<Message> getSentMessages(@RequestParam(required = false, name = "start", defaultValue = "0") int start,
                                         @RequestParam(required = false, name = "size", defaultValue = "10") int size,
                                         HttpServletRequest request) throws ValidationException {
        return messageService.getSentMessages(request.getUserPrincipal().getName(),
                PageRequest.of(start, size, Sort.by(new Sort.Order(Sort.Direction.DESC, "sendTime"))));
    }


    @GetMapping("receive/unread")
    public Page<Message> getUnreadMessages(@RequestParam(required = false, name = "start", defaultValue = "0") int start,
                                           @RequestParam(required = false, name = "size", defaultValue = "10") int size,
                                           HttpServletRequest request) throws ValidationException {
        return messageService.getUnreadMessages(request.getUserPrincipal().getName(),
                PageRequest.of(start, size, Sort.by(new Sort.Order(Sort.Direction.DESC, "sendTime"))));
    }

    @PostMapping("delete")
    public void deleteMessage(@RequestParam(name = "messageId") long messageId, HttpServletRequest request) {
        messageService.deleteMessage(request.getUserPrincipal().getName(), messageId);
    }
}

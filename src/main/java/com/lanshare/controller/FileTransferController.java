package com.lanshare.controller;

import com.lanshare.domain.File;
import com.lanshare.domain.FileAccess;
import com.lanshare.enums.FileAction;
import com.lanshare.exception.ValidationException;
import com.lanshare.service.FileAccessService;
import com.lanshare.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.stream.Collectors;

@RequestMapping("/file-service")
@RestController
public class FileTransferController {

    @Autowired
    private FileService fileService;

    @Autowired
    private FileAccessService fileAccessService;

    @GetMapping("/browse")
    public ResponseEntity showDownloads(@RequestParam(required = false, name = "path", defaultValue = "") String path,
                                        HttpServletRequest httpServletRequest) throws IOException {
        return ResponseEntity.ok().body(
                fileService.getFileList(path).stream().
                        map(e -> new File(e.getName(), e.isFile(), e.length(), e.lastModified())).
                        collect(Collectors.toList())
        );
    }

    @GetMapping("/download")
    public ResponseEntity<InputStreamResource> download(@RequestParam(name = "filepath") String filepath,
                                                        HttpServletRequest request) throws IOException, ValidationException {
        java.io.File file;
        InputStreamResource resource;

        file = fileService.getFile(filepath);
        resource = new InputStreamResource(new FileInputStream(file));

        fileAccessService.addAccess(new FileAccess(request.getUserPrincipal().getName(), file.getName(),
                String.valueOf(file.length()), file.getParent(),
                FileAction.DOWNLOAD.name()));

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment;filename=\"" + file.getName() + "\"")
                .contentType(MediaType.APPLICATION_OCTET_STREAM).contentLength(file.length())
                .body(resource);
    }

    @PostMapping("/upload")
    public ResponseEntity uploadFile(@RequestParam(value = "file") MultipartFile file,
                                     HttpServletRequest request) throws ValidationException {
        try {
            fileService.uploadFile(fileService.getDefaultUploadDir() + java.io.File.separator +
                    request.getUserPrincipal().getName(), file);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        fileAccessService.addAccess(new FileAccess(request.getUserPrincipal().getName(), file.getOriginalFilename(),
                String.valueOf(file.getSize()),
                fileService.getDefaultUploadDir(), FileAction.UPLOAD.name()));

        return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "text/plain").build();
    }
}

package com.lanshare.repository;

import com.lanshare.entity.MessageEntity;
import com.lanshare.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<MessageEntity, Long> {

    Page<MessageEntity> findMessagesByReceiver(UserEntity receiver, Pageable limit);

    Page<MessageEntity> findMessagesBySender(UserEntity sender, Pageable limit);

    Page<MessageEntity> findMessagesByReceiverAndStatus(UserEntity receiver, String status, Pageable limit);

    int countMessagesByReceiverAndStatus(UserEntity receiver, String status);

}

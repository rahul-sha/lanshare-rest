package com.lanshare.repository;

import com.lanshare.entity.UserIpEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserIpRepository extends CrudRepository<UserIpEntity, Long> {

    Optional<UserIpEntity> findByIp(String ip);

}

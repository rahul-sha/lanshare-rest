package com.lanshare.repository;

import com.lanshare.entity.LoginLogEntity;
import com.lanshare.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoginLogRepository extends JpaRepository<LoginLogEntity, Long> {

    Page<LoginLogEntity> findAll(Pageable limit);

    Page<LoginLogEntity> findAllByUser(UserEntity userEntity, Pageable limit);
}

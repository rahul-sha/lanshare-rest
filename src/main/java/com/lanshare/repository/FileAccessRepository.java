package com.lanshare.repository;

import com.lanshare.entity.FileAccessEntity;
import com.lanshare.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FileAccessRepository extends CrudRepository<FileAccessEntity, Long> {

    Page<FileAccessEntity> findAll(Pageable limit);

    Page<FileAccessEntity> findByUserAndAction(UserEntity user, String action, Pageable limit);
}

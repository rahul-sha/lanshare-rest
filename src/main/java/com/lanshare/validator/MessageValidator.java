package com.lanshare.validator;

import com.lanshare.domain.Message;
import com.lanshare.exception.ValidationException;
import com.lanshare.validator.base.Validator;
import org.springframework.stereotype.Component;

import static com.lanshare.validator.base.ValidationUtility.validateTrue;

@Component
public class MessageValidator implements Validator<Message> {

    private static final int MAX_MESSAGE_LENGTH = 255;

    @Override
    public void validate(Message message) throws ValidationException {
        validateTrue(message.getReceiverId() != null, "ReceiverId can not be null");
        validateTrue(message.getSenderId() != null, "SenderId can not be null");
        validateTrue(message.getMessageBody().length() > 0, "Message is empty");
        validateTrue(message.getMessageBody().length() <= MAX_MESSAGE_LENGTH, "Message is too long");
    }

}

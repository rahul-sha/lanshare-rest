package com.lanshare.validator.base;

import com.lanshare.exception.ValidationException;

public interface Validator<T> {

    void validate(T target) throws ValidationException;

}

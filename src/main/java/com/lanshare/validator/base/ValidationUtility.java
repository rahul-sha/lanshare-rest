package com.lanshare.validator.base;

import com.lanshare.exception.ValidationException;

public class ValidationUtility {

    public static void validateTrue(Boolean aBoolean, String errorMessage) throws ValidationException {
        if (!aBoolean) {
            throw new ValidationException(errorMessage);
        }
    }
}

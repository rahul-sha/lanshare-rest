package com.lanshare.service;

import com.lanshare.domain.LoginLog;
import com.lanshare.entity.LoginLogEntity;
import com.lanshare.entity.UserEntity;
import com.lanshare.repository.LoginLogRepository;
import com.lanshare.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Service
@Validated
public class LoginLogService {

    @Autowired
    private LoginLogRepository loginLogRepository;

    @Autowired
    private UserRepository userRepository;

    public void addLoginLog(@NotNull LoginLog loginLog){
        Optional<UserEntity> userEntity = userRepository.findUserByName(loginLog.getUsername());
        assert userEntity.isPresent() : "User not found";

        LoginLogEntity loginLogEntity = new LoginLogEntity();
        loginLogEntity.setUser(userEntity.get());
        loginLogEntity.setDetails(loginLog.getDetails());
        loginLogEntity.setDatetime(loginLog.getDatetime());
    }

    public Page<LoginLog> getLoginLogs(@NotNull Pageable limit){
        return loginLogRepository.findAll(limit).map(LoginLogEntity::getDto);
    }

    public Page<LoginLog> getLoginLogs(@NotNull String username, @NotNull Pageable limit){
        Optional<UserEntity> userEntity = userRepository.findUserByName(username);
        assert userEntity.isPresent() : "User not found";
        return loginLogRepository.findAllByUser(userEntity.get(), limit).map(LoginLogEntity::getDto);
    }

}

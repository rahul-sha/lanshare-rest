package com.lanshare.service;

import com.lanshare.domain.Message;
import com.lanshare.entity.MessageEntity;
import com.lanshare.entity.UserEntity;
import com.lanshare.enums.MessageStatus;
import com.lanshare.exception.ValidationException;
import com.lanshare.repository.MessageRepository;
import com.lanshare.repository.UserRepository;
import com.lanshare.validator.MessageValidator;
import com.lanshare.validator.base.ValidationUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.lanshare.validator.base.ValidationUtility.validateTrue;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Service
public class MessageService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private MessageValidator messageValidator;

    @Transactional
    public void writeMessage(Message message) throws ValidationException {
        messageValidator.validate(message);

        Optional<UserEntity> sender = userRepository.findUserByName(message.getSenderId());
        Optional<UserEntity> receiver = userRepository.findUserByName(message.getReceiverId());

        validateTrue(sender.isPresent(), "Invalid sender id");
        validateTrue(receiver.isPresent(), "Invalid receiver id");

        MessageEntity messageEntity = new MessageEntity(sender.get(), receiver.get(), message.getMessageBody(),
                                                        message.getSendTime(), message.getStatus());

        messageRepository.save(messageEntity);
    }

    @Transactional(readOnly = true)
    public Page<Message> getMessages(@NotNull String receiverId, @NotNull Pageable limit) throws ValidationException {
        Optional<UserEntity> receiver = userRepository.findUserByName(receiverId);

        validateTrue(receiver.isPresent(), "Invalid receiver id");

        return messageRepository.findMessagesByReceiver(receiver.get(), limit)
                .map(MessageEntity::getDto);
    }

    @Transactional(readOnly = true)
    public Page<Message> getSentMessages(@NotNull String receiverId, @NotNull Pageable limit) throws ValidationException {
        Optional<UserEntity> receiver = userRepository.findUserByName(receiverId);

        validateTrue(receiver.isPresent(), "Invalid receiver id");

        return messageRepository.findMessagesBySender(receiver.get(), limit)
                .map(MessageEntity::getDto);
    }

    @Transactional(readOnly = true)
    public Page<Message> getUnreadMessages(@NotNull String receiverId, @NotNull Pageable limit) throws ValidationException {
        Optional<UserEntity> receiver = userRepository.findUserByName(receiverId);

        validateTrue(receiver.isPresent(), "Invalid receiver id");

        return messageRepository.findMessagesByReceiverAndStatus(receiver.get(), MessageStatus.UNREAD.name(), limit)
                .map(MessageEntity::getDto);
    }

    @Transactional
    public void deleteMessage(@NotNull String receiverId, @NotNull long messageId) {
        Optional<MessageEntity> message = messageRepository.findById(messageId);

        assert message.isPresent() : "Invalid message Id";
        assert message.get().getSender().getName().equals(receiverId) : "Message deletion not allowed";

        messageRepository.deleteById(messageId);
    }

    @Transactional(readOnly = true)
    public int getUnreadMessageCount(@NotNull String receiverId) {
        Optional<UserEntity> receiver = userRepository.findUserByName(receiverId);

        assert receiver.isPresent() : "Invalid receiver id";

        return messageRepository.countMessagesByReceiverAndStatus(receiver.get(), MessageStatus.UNREAD.name());
    }
}

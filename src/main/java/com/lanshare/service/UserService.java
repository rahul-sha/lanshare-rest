package com.lanshare.service;

import com.lanshare.domain.User;
import com.lanshare.entity.UserEntity;
import com.lanshare.entity.UserIpEntity;
import com.lanshare.exception.UserNotFoundException;
import com.lanshare.repository.UserIpRepository;
import com.lanshare.repository.UserRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class UserService {

    private final static Logger logger = Logger.getLogger(UserService.class);

    @Autowired
    private UserIpRepository userIpRepository;

    @Autowired
    private UserRepository userRepository;

    @Transactional(readOnly = true)
    public User getUserFromIp(String ip) throws UserNotFoundException {
        Optional<UserIpEntity> optionalUserIpEntity = userIpRepository.findByIp(ip);

        if (!optionalUserIpEntity.isPresent()) {
            logger.warn(String.format("Failed to retrieve user from IP [%s]", ip));
            throw new UserNotFoundException(String.format("No userEntity exists with IP [%s]", ip));
        }
        return optionalUserIpEntity.get().getUserEntity().getDto();
    }

    @Transactional(readOnly = true)
    public User getUserByName(String name) {
        Optional<UserEntity> optionalUserEntity = userRepository.findUserByName(name);
        assert optionalUserEntity.isPresent() : "User not found";
        return optionalUserEntity.get().getDto();
    }

    @Transactional(readOnly = true)
    public List<User> getAllUsers() {
        return userRepository.findAll().stream().map(UserEntity::getDto).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<String> getAllUserNames() {
        return userRepository.findAll().stream().map(UserEntity::getName).collect(Collectors.toList());
    }
}

package com.lanshare.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.io.File.separator;

@Service
@PropertySource("classpath:environment.properties")
public class FileService {

    private final String LANSHARE_HOME;

    private final String DOWNLOAD_DIR;

    private final String UPLOAD_DIR;

    @Autowired
    FileService(@NotNull Environment environment) {
        LANSHARE_HOME = environment.getProperty("lanshare.home");
        DOWNLOAD_DIR = environment.getProperty("download.dir");
        UPLOAD_DIR = environment.getProperty("upload.dir");
    }


    public List<File> getFileList(@NotNull String path) throws IOException {
        File directory = new File(LANSHARE_HOME + separator + DOWNLOAD_DIR + separator + path);
        validatePath(directory.toPath());

        if (!directory.exists() || directory.isFile())
            throw new FileNotFoundException(String.format("Invalid path [%s]", path));

        File[] files = directory.listFiles();
        if (files == null)
            return Collections.emptyList();

        List<File> fileList = Arrays.asList(files);
        fileList.sort((o1, o2) -> o1.isFile() ^ o2.isFile() ? (o1.isFile() ? 1 : -1) : 1);
        return fileList;
    }

    public File getFile(@NotNull String filePath) throws IOException {
        File file = new File(LANSHARE_HOME + separator + DOWNLOAD_DIR + separator + filePath);
        validatePath(file.toPath());

        if (!file.exists() || !file.isFile())
            throw new FileNotFoundException(filePath + "not found");

        return file;
    }

    public void uploadFile(@NotNull String path, @NotNull MultipartFile file) throws IOException {
        File parentDirectory = new File(LANSHARE_HOME + separator + path);
        if (!parentDirectory.exists() && parentDirectory.mkdirs()) {
            throw new IOException("Failed to create the parent directory(s)");
        }
        File targetFile = new File(parentDirectory.getPath() + separator + file.getOriginalFilename());
        file.transferTo(targetFile);
    }

    /**
     * Uploads a file to the default upload location
     *
     * @param file The file to be uploaded
     * @throws IOException If upload fails
     */
    public void uploadFile(@NotNull MultipartFile file) throws IOException {
        uploadFile(UPLOAD_DIR, file);
    }

    public String getDefaultUploadDir(){
        return UPLOAD_DIR;
    }

    private void validatePath(Path path) throws IOException {
        path = path.normalize();
        if (path.toString().length() < LANSHARE_HOME.length() || !path.startsWith(LANSHARE_HOME)) {
            throw new IOException("Invalid path provided");
        }
    }

}

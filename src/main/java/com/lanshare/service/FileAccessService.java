package com.lanshare.service;

import com.lanshare.domain.FileAccess;
import com.lanshare.entity.FileAccessEntity;
import com.lanshare.entity.UserEntity;
import com.lanshare.enums.FileAction;
import com.lanshare.exception.ValidationException;
import com.lanshare.repository.FileAccessRepository;
import com.lanshare.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.lanshare.validator.base.ValidationUtility.validateTrue;

@Service
public class FileAccessService {

    private static final String FILE_ACCESS_SORT_PROPERTY = "fileAccessPk";

    @Autowired
    private FileAccessRepository fileAccessRepository;

    @Autowired
    private UserRepository userRepository;

    public void addAccess(FileAccess fileAccess) throws ValidationException {
        Optional<UserEntity> userEntity = userRepository.findUserByName(fileAccess.getUsername());
        validateTrue(userEntity.isPresent(), "Illegal file access. Username is not known");
        FileAccessEntity fileAccessEntity = new FileAccessEntity(userEntity.get(), fileAccess.getFilename(),
                fileAccess.getFilesize(), fileAccess.getLocation(), fileAccess.getAction(), fileAccess.getDatetime());
        fileAccessRepository.save(fileAccessEntity);
    }

    public Page<FileAccess> getFileAccesses(int start, int size) {
        return fileAccessRepository.findAll(PageRequest.of(start, size,
                Sort.by(new Sort.Order(Sort.Direction.DESC, FILE_ACCESS_SORT_PROPERTY))))
                .map(FileAccessEntity::getDto);
    }

    public Page<FileAccess> getAccessesOf(String username, FileAction action, int start, int size) {
        Optional<UserEntity> userEntity = userRepository.findUserByName(username);
        if (!userEntity.isPresent()) throw new AssertionError("Invalid username");
        return fileAccessRepository.findByUserAndAction(userEntity.get(), action.name(), PageRequest.of(start, size,
                Sort.by(new Sort.Order(Sort.Direction.DESC, FILE_ACCESS_SORT_PROPERTY))))
                .map(FileAccessEntity::getDto);
    }

}

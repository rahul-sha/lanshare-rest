package com.lanshare.domain;

public class File {
    private String name;
    private boolean isFile;
    private long length;
    private long lastModified;

    public File(String name, boolean isFile, long length, long lastModified) {
        this.name = name;
        this.isFile = isFile;
        this.length = length;
        this.lastModified = lastModified;
    }

    public String getName() {
        return name;
    }

    public boolean isFile() {
        return isFile;
    }

    public long getLength() {
        return length;
    }

    public long getLastModified() {
        return lastModified;
    }
}

package com.lanshare.domain;

public class LoginLog {

    private String username;

    private String datetime;

    private String details;

    public LoginLog(String username, String datetime, String details) {
        this.username = username;
        this.datetime = datetime;
        this.details = details;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

}

package com.lanshare.domain;

import com.lanshare.enums.ErrorLevel;

public class Error {

    private String errorMessage;
    private ErrorLevel errorLevel;

    public Error(ErrorLevel errorLevel, String errorMessage) {
        this.errorMessage = errorMessage;
        this.errorLevel = errorLevel;
    }

    public Error(String errorMessage) {
        this(ErrorLevel.MINOR, errorMessage);
    }

    //<editor-fold desc="Getters and Setters">
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ErrorLevel getErrorLevel() {
        return errorLevel;
    }

    public void setErrorLevel(ErrorLevel errorLevel) {
        this.errorLevel = errorLevel;
    }
    //</editor-fold>
}

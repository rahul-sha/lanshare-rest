package com.lanshare.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface EntityMappable<T> {

    /*
    *   JsonIgnore annotation added to ignore this getter when
    *   converting to json by jackson
    */
    @JsonIgnore
    T getEntity();
}

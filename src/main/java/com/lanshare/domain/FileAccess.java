package com.lanshare.domain;

public class FileAccess {

    private String username;

    private String filename;

    private String filesize;

    private String location;

    private String action;

    private String datetime;

    public FileAccess(String username, String filename, String filesize, String location, String action) {
        this(username, filename, filesize, location, action, String.valueOf(System.currentTimeMillis()));
    }

    public FileAccess(String username, String filename, String filesize, String location,
                      String action, String datetime) {
        this.username = username;
        this.filename = filename;
        this.filesize = filesize;
        this.location = location;
        this.action = action;
        this.datetime = datetime;
    }

    //<editor-fold desc="Getters and Setters">
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilesize() {
        return filesize;
    }

    public void setFilesize(String filesize) {
        this.filesize = filesize;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    //</editor-fold>
}

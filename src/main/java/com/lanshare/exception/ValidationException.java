package com.lanshare.exception;

/**
 * This Exception should be throw if some validation fails, as the name suggests.
 * It will correspond to client error 40X
 */

public class ValidationException extends LSException {

    public ValidationException(String message) {
        super(message);
    }

}

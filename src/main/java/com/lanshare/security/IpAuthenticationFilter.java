package com.lanshare.security;

import org.apache.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class IpAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private final static Logger logger = Logger.getLogger(IpAuthenticationFilter.class);

    public IpAuthenticationFilter(String defaultFilterProcessesUrl) {
        super(defaultFilterProcessesUrl);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
            throws AuthenticationException {
        logger.info(String.format("Authentication request intercepted from : [%s]", httpServletRequest.getRemoteAddr()));
        return this.getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken(null, httpServletRequest.getRemoteAddr()));
    }

}
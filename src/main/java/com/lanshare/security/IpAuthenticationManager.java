package com.lanshare.security;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public class IpAuthenticationManager implements AuthenticationManager {

    private IpAuthenticationProvider ipAuthenticationProvider;

    public IpAuthenticationManager(IpAuthenticationProvider ipAuthenticationProvider){
        this.ipAuthenticationProvider = ipAuthenticationProvider;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        return ipAuthenticationProvider.authenticate(authentication);
    }
}

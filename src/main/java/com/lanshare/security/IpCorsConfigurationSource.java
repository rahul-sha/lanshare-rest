package com.lanshare.security;

import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

public class IpCorsConfigurationSource implements CorsConfigurationSource {

    @NotNull
    private CorsConfiguration corsConfiguration;

    public IpCorsConfigurationSource(CorsConfiguration corsConfiguration) {
        this.corsConfiguration = corsConfiguration;
    }

    @Override
    public CorsConfiguration getCorsConfiguration(HttpServletRequest httpServletRequest) {
        return corsConfiguration;
    }
}

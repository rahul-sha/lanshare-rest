package com.lanshare.configuration;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/*
*   This is a WebApplicationInitializer responsible for setting up the Spring Security filters
*/
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
}

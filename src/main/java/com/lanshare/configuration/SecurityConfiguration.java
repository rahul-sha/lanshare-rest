package com.lanshare.configuration;

import com.lanshare.security.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

@EnableWebSecurity
@PropertySource(value = {"classpath:security.properties"})
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private Environment env;

    protected void configure(HttpSecurity http) throws Exception {
        System.out.println("Security config");
        http.authorizeRequests()
                .antMatchers("/admin/**").hasAuthority("ROLE_ADMIN")
                .antMatchers("/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_USER")
                .anyRequest().authenticated()
                .and()
                .csrf().disable()
                .addFilterAt(ipAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
                .cors().configurationSource(corsConfigurationSource())
                .and()
                .exceptionHandling().authenticationEntryPoint(entryPoint());
    }

    @Bean
    public IpAuthenticationFilter ipAuthenticationFilter(){
        IpAuthenticationFilter filter =  new IpAuthenticationFilter("/login");
        filter.setAuthenticationManager(authenticationManager());
        filter.setAuthenticationSuccessHandler(authenticationSuccessHandler());
        filter.setAuthenticationFailureHandler(authenticationFailureHandler());
        return filter;
    }

    @Bean
    public AuthenticationManager authenticationManager(){
        return new IpAuthenticationManager(authenticationProvider());
    }

    @Bean
    public IpAuthenticationProvider authenticationProvider(){
        return new IpAuthenticationProvider();
    }

    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler(){
        return new IpAuthenticationSuccessHandler();
    }

    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler(){
        return new IpAuthenticationFailureHandler();
    }

    @Bean
    public AuthenticationEntryPoint entryPoint(){
        return new IpAuthenticationEntryPoint();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource(){
        return new IpCorsConfigurationSource(corsConfiguration());
    }

    @Bean
    public CorsConfiguration corsConfiguration(){
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowedMethods(Arrays.asList(
                Objects.requireNonNull(env.getProperty("security.allowed.methods")).split(",")));
        corsConfiguration.setAllowedHeaders(Collections.singletonList("*"));
        corsConfiguration.setAllowedOrigins(Arrays.asList(Objects.requireNonNull(env.getProperty("security.allowed.origins")).split(",")));
        corsConfiguration.setAllowCredentials(Boolean.parseBoolean(env.getProperty("security.allow.credentials")));
        return corsConfiguration;
    }
}

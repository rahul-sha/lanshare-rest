package com.lanshare.enums;

public enum ErrorLevel {
    MINOR,
    MAJOR,
    CRITICAL
}

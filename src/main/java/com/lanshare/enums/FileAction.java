package com.lanshare.enums;

public enum FileAction {
    DOWNLOAD,
    UPLOAD,
    DELETE,
    MOVE,
    COPY;
}

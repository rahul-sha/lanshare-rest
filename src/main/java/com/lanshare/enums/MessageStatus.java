package com.lanshare.enums;

public enum MessageStatus {
    READ,
    UNREAD;
}

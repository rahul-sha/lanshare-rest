package com.lanshare.enums;

public enum UserAuthorities {
    ROLE_ADMIN,
    ROLE_USER,
    READ_USER,
    WRITE_USER,
    DELETE_USER,
    UPLOAD_FILE,
    DOWNLOAD_FILE;
}
